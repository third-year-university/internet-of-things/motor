bool rotate(bool clockwise, byte speed, int clockwise_pin, int counterclockwise_pin){
  if (clockwise){
    analogWrite(clockwise_pin, speed);
  }
  else{
    analogWrite(counterclockwise_pin, speed);
  }
}

void setup() {
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
}

void loop() {
  for (int vel = 0; vel <= 255; vel++){
    rotate(true, vel, 3, 5);
    delay(20);
  }
  delay(5000);
  for (int vel = 255; vel > 0; vel--){
    rotate(true, vel, 3, 5);
    delay(20);
  }
  for (int vel = 0; vel <= 255; vel++){
    rotate(false, vel, 3, 5);
    delay(20);
  }
  delay(5000);
  for (int vel = 255; vel > 0; vel--){
    rotate(false, vel, 3, 5);
    delay(20);
  }
}
